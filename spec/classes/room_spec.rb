require "spec_helper"

describe Room do
  before(:each) do 
    @name = 'Living Room'
    @description = 'Grey walls and shabby carpets lend an element of despair to the room. A TV, Sofa and Coffee table are here.'
    @room = Room.new(@name, @description)
  end

  it "should remember its name" do
    @room.name.should eq @name
  end

  it "should remember its description" do
    @room.description.should include "Grey"
  end

  it "should have available directions"

  describe "treasure" do
    before(:each) do
      @treasures = {:sword => "gold", :goblet => "of fire"}
      @room = Room.new "Throne Room", "filled with riches, with an empty throne at the north wall", @treasures
    end

    it "should have treasure" do
      @room.has_treasure?.should eq true
    end

    it "should show a list of treasures in the room" do
      @room.describe_treasures.should include "flaming"
    end

    it "should be possible to take a treasure"
    it "should modify its description based on treasure in the room"
  end
end