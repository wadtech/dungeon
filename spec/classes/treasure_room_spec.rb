require 'room'
require 'treasure_room'

describe TreasureRoom do

  before(:each) do
    @treasures = {:sword => "flaming sword", :goblet => "of fire"}
    @room = TreasureRoom.new "Throne Room", "filled with riches, with an empty throne at the north wall", @treasures
  end

  it "should have treasure" do
    @room.treasures.should_not be_nil
  end

  it "should show a list of treasures in the room" do
    @room.describe_treasures.should contain "flaming"
  end
  it "should be possible to take a treasure"
  it "should modify its description based on treasure in the room"
end