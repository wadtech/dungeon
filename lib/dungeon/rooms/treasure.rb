module Dungeon 
  class Treasure
    attr_accessor :name, :description, :value, :location


    def initialize(name, description, value, location=nil)
      @name, @description, @value = name, description, value
      @location = location unless location.nil?
    end

    def to_s
      print "#{name}\t#{description}\t#{value}\n" }
    end

    #changes location of object to the player's inventory
    def take_treasure
      @location = Player.instance.inventory
    end

    #move treasure to player's current room
    def drop_treasure
      @location = Player.instance.location
    end

    def self.get_player
      return self
    end
  end
end