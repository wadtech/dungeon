module Dungeon
  class Room
    attr_accessor :name, :description, :paths
    
    def initialize(name, description, treasures=nil)
      @name = name
      @description = description
      @treasures ||= treasures
      @paths = {}
    end

    def go(direction)
      @paths[direction]
    end

    def add_paths(paths)
      @paths.update(paths)
    end

    def has_treasure?
      not @treasures.empty?
    end

    def describe_treasure(treasure)
      treasure.each {|key, value| puts "#{key} #{value}"}
    end

    def describe_treasures
      puts "There are #{@treasures.size} treasures in this room."
      @treasures.each { |treasure| describe_treasure(treasure) }
      puts
    end

    #returns object taken from room (hash with itemname/description)
    def take_treasure(treasure)
      item = @treasures.select(treasure)
      @treasures = @treasures.reject(treasure)
      item
    end

    def describe_treasures
      local_treasure = Treasure.find({:location => self}).size
      puts "There are #{local_treasure.size} treasures in this room."
      local_treasure.each do { |treasure| treasure.to_s }
      puts
    end
  end
end