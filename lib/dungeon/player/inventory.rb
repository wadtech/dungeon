module Dungeon
  class Inventory
    attr_reader :total_value

    def initialize
      to_s
    end

    def total_value
      items = Treasure.find({:location => self})
      items.each.inject(0) { |item, total| total += item.value }
    end

    def to_s
      items = Treasure.find({:location => self})
      if items.empty?
        "Nothing in inventory."
      else
        items.each.inject(0) { |item, count| puts "#{count}. #{item.name}\t#{item.value}G\n#{item.description}"}
        puts "Total value of inventory: #{total_value}"
      end
    end
  end
end