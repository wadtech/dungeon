require 'singleton'

class Player
  include Singleton

  attr_reader :name, :age, :health
  attr_accessor :inventory, :location

  def initialize
    puts "New Player..."
    puts "===================================="
    loop do
      print "What is your name? "
      @name = gets.chomp()
      puts
      print "What is your age? "
      @age = gets.chomp()
      puts
      puts
      puts "You are #{@name} and are #{@age}. Is this correct? (Y/n)"
      response = gets.chomp()
      if response == "Y" || response == "y" || response.empty?
        break
      end
    end

    @inventory = Inventory.new(nil)
    @health = 10
    @location = nil
  end

  def hurt(amount)
    @health -= amount
    if @health < 5
      puts "feeling grim"
    elsif @health < 1
      self.death
    end
  end

  def heal(amount)
    @health += amount
    if @health < 5
      puts "feeling better, but still hurting"
    elsif @health < 10
      puts "feeling OK"
    else
      puts "feeling great"
  end

private

  def death
    puts "You have died. Whoops!"
    System.exit(1)
  end
end