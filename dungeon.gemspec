# -*- encoding: utf-8 -*-
$:.push File.expand_path("../lib", __FILE__)
require "dungeon/version"

Gem::Specification.new do |s|
  s.name        = "Dungeon"
  s.version     = Dungeon::VERSION
  s.authors     = ["Peter Mellett"]
  s.email       = ["contact@petermellett.co.uk"]
  s.homepage    = "http://www.petermellett.co.uk"
  s.summary     = <<-SUMMARY
  A simple dungeon crawler game to practice Object-Oriented design in the Ruby language.
  SUMMARY
  s.description = <<-DESC
  Dungeon uses Ruby 1.9.3 to create a simple text game which features persistent inventory, health and puzzle solving.
  DESC
  # s.rubyforge_project = "NAME"

  s.files         = `git ls-files`.split("\n")
  s.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
  s.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  s.require_paths = ["lib"]
end